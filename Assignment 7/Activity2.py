def Calculate(hours, rate):
    if hours > 40:
        grossPay = 40 * rate + (hours - 40) * 1.5 * rate
    else:
        grossPay = rate * hours
    return grossPay

# Main
print("Enter Hours value")
hours = int(input())
print("Enter the hourly rate")
rate = float(input())
grossPay = Calculate(hours, rate)
print("Gross pay is " + str(grossPay))
