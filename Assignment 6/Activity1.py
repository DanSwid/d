# Comment goes here...

def input_hours():
    print("Enter Hours:")
    hours = int(input())
    return hours

def input_rate():
    print("Enter Rate:")
    rate = int(input())
    return rate

def calculate_gross(hours, rate):
    gross = hours * rate
    return gross

def display_result(gross):
    print("Gross Pay = " + str(gross))

def main():
    hours = input_hours()
    rate = input_rate()
    gross = calculate_gross(hours, rate)
    display_result(gross)

main()
