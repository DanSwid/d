# Comment goes here.

def get_value():
    print("Enter the value")
    value = int(input())
    return value

def get_count():
    print("Enter the number of expressions to be displayed")
    count = int(input())
    return count


def for_loop(value, count):
    print("Expressions Using For Loop: ")
    for index in range(1, count + 1):
        product = value * index
        display_results(value, index, product)


def while_loop(value, count):
    print("Expressions Using While Loop: ")
    index = 1
    while index <= count:
        product = value * index
        display_results(value, index, product)
        index = index + 1


def display_results(value, index, product):
    print(str(value) + " * " + str(index) + " = " + str(product))


def main():
    value = get_value()
    count = get_count()
    while_loop(value, count)
    for_loop(value, count)


main()
