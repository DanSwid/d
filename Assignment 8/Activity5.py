<?xml version="1.0"?>
<flowgorithm fileversion="2.0">
    <attributes>
        <attribute name="name" value=""/>
        <attribute name="authors" value="Saurabh"/>
        <attribute name="about" value=""/>
        <attribute name="saved" value="2017-03-06 10:33:40 AM"/>
        <attribute name="created" value="U2F1cmFiaDsgU0FVUkFCSC1QQzsgMjAxNy0wMy0wNjsgMTA6MDk6NDkgQU07IDI3ODc="/>
        <attribute name="edited" value="U2F1cmFiaDsgU0FVUkFCSC1QQzsgMjAxNy0wMy0wNjsgMTA6MzM6NDAgQU07IDI3NzU="/>
    </attributes>
    <function name="Main" type="None" variable="">
        <parameters/>
        <body>
            <declare name="year" type="Integer" array="False" size="" variables="year"/>
            <declare name="month" type="Integer" array="False" size="" variables="month"/>
            <declare name="day" type="Integer" array="False" size="" variables="day"/>
            <output expression="&quot;Enter year, month, day &quot;"/>
            <input variable="year"/>
            <input variable="month"/>
            <input variable="day"/>
            <call expression="printday(year,month,day)"/>
        </body>
    </function>
    <function name="printday" type="None" variable="">
        <parameters>
            <parameter name="year" type="Integer" array="False"/>
            <parameter name="month" type="Integer" array="False"/>
            <parameter name="day" type="Integer" array="False"/>
        </parameters>
        <body>
            <if expression="month&lt;3">
                <then>
                    <assign variable="month" expression="month+12"/>
                    <assign variable="year" expression="year-1"/>
                </then>
                <else/>
            </if>
            <declare name="h" type="Integer" array="False" size="" variables="h"/>
            <declare name="K" type="Integer" array="False" size="" variables="K"/>
            <declare name="K4" type="Integer" array="False" size="" variables="K4"/>
            <assign variable="K" expression="year%100"/>
            <assign variable="K4" expression="K/4"/>
            <declare name="J" type="Integer" array="False" size="" variables="J"/>
            <declare name="J4" type="Integer" array="False" size="" variables="J4"/>
            <assign variable="J" expression="year/100"/>
            <assign variable="J4" expression="J/4"/>
            <declare name="temp" type="Integer" array="False" size="" variables="temp"/>
            <assign variable="temp" expression="(13*(month+1))/5"/>
            <assign variable="h" expression="(day+temp+K+K4+J4-2*J)%7"/>
            <if expression="h&gt;3">
                <then>
                    <if expression="h=6">
                        <then>
                            <output expression="&quot;Friday&quot;"/>
                        </then>
                        <else>
                            <if expression="h=5">
                                <then>
                                    <output expression="&quot;Thursday&quot;"/>
                                </then>
                                <else>
                                    <output expression="&quot;Wednesday&quot;"/>
                                </else>
                            </if>
                        </else>
                    </if>
                </then>
                <else>
                    <if expression="h=0">
                        <then>
                            <output expression="&quot;Saturday&quot;"/>
                        </then>
                        <else>
                            <if expression="h=1">
                                <then>
                                    <output expression="&quot;Sunday&quot;"/>
                                </then>
                                <else>
                                    <if expression="h=2">
                                        <then>
                                            <output expression="&quot;Monday&quot;"/>
                                        </then>
                                        <else>
                                            <output expression="&quot;Tuesday&quot;"/>
                                        </else>
                                    </if>
                                </else>
                            </if>
                        </else>
                    </if>
                </else>
            </if>
        </body>
    </function>
</flowgorithm>