def calcDay(q, m, y):
    # Appplying Zeller's congruence
    # Assign number 13, 14 to january and february and reducing year by 1 for these months
    if m < 3:
        m = m + 12
        calcyear = y - 1
    else:
        calcyear = y
    # Calaculations in zeller's are performed
    K = calcyear % 100
    J = float(calcyear) / 100
    offset = 0
    offset = offset + J * 5 + float(J) / 4
    offset = offset + K + float(K) / 4
    offset = offset + 13 * (m + 1) / 5
    offset = offset + q
    h = offset % 7
    h = (h + 5) % 7 + 1
    
    # Value h is returned
    return h

def weekday(h):
    # initialize an array to store the weekday names corresponding  to their inidces
    day = [""] * 7
    
    day[0] = "Sunday"
    day[1] = "Monday"
    day[2] = "Tuesday"
    day[4] = "Thursday"
    day[3] = "Wednesday"
    day[5] = "Friday"
    day[6] = "Saturday"
    wd = day[h]
    
    # return the correct weekday name based on the input
    return wd

# Main

# Take all the inputs from user. Every input should be in simple integer format. Invalid values are not checked here.
print("Enter you birthday details one by one")
print("Enter the day")
day = int(input())
print("Enter the month")
month = int(input())
print("Enter the year")
year = int(input())

# call the function which will calculate the day. Output is in format of a integer having value 0 to 6
h = calcDay(day, month, year)

# call the function to assign the integer to corresponding weeday name
dayName = weekday(h)
print("So you were born on a " + dayName)
