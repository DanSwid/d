Module main()
    Declare Integer N
    
    // Here the function input is called to take the input, number of scores
    Set N = input_function()
    Declare Integer Scores[N]
    Declare Integer i
    
    For i = 0 To N - 1
        Display "Enter the Score for student number ", i + 1
        Input Scores[i]
    End For
    
    // Function summary is called by passing the array and its size to calculate the minimum, maximum and average grade.
    Call summary(Scores)
End Module

Function Integer input_function()
    Declare Integer N
    
    Display "How many scores you want to enter?"
    Input N
    
    // Return the value N to main function
    
    Return N
End Function

Module output(Integer min, Integer max, Integer sum, Integer size)
    Declare Real Average
    
    // Calculate the average of the grades
    Set Average = sum / size
    Display "Minimum Grade : ", min
    Display "Maximum Grade : ", max
    Display "Average Grade : ", Average
End Module

Module summary(Integer arr[])
    Declare Integer size
    
    // Find the size of array using in built function
    Set size = size(arr)
    Declare Integer Max
    
    Set Max = 1
    Declare Integer Min
    
    // Initialize the min with a veru high value
    Set Min = 32000
    Declare Integer Sum
    
    Set Sum = 0
    Declare Integer i
    
    For i = 0 To size - 1
        If arr[i] > Max Then
            // if current value is higher than maximum of past elements, make the new element as max
            Set Max = arr[i]
        Else
            If arr[i] < Min Then
                Set Min = arr[i]
            Else
                // if current value is lesser than minimum of past elements, make the new element as min
            End If
        End If
        
        // Sum of all the elements of array
        Set Sum = Sum + arr[i]
    End For
    
    // call the output function to display the result
    Call output(Min, Max, Sum, size)
End Module
