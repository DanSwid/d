import math
class ShaeArea:
	def areaTriangle(self,b,h):
		if(b>0 and h>0):
			return 0.5*b*h
		else:
			raise InputError("Invalid values for calculating area")
	def areaRectangle(self,w,h):
		if(w>0 and h>0):
			return w*h
		else:
			raise InputError("Invalid values for calculating area")
	def areaSquare(self,a):
		if(a>0):
			return a*a
		else:
			raise InputError("Invalid values for calculating area")
	def areaParallelogram(self,b,h):
		if(b>0 and h>0):
			return b*h
		else:
			raise InputError("Invalid values for calculating area")
	def areaTrapezium(self,a,b,h):
		if(a>0 and b>0 and h>0):
			return 0.5*(a+b)*h
		else:
			raise InputError("Invalid values for calculating area")
	def areaCircle(self,r):
		if(r>0):
			return math.pi*r*r
		else:
			raise InputError("Invalid values for calculating area")
	def areaEllipse(self,a,b):
		if(a>0 and b>0):
			return math.pi*a*b
		else:
			raise InputError("Invalid values for calculating area")
	def areaSector(self,r,theta):
		if(r>0 and theta>0):
			return 0.5*r*r*theta
		else:
			raise InputError("Invalid values for calculating area")


def main():
	print("What shape would you like to calculate the area for")
	print("1. Triangle")
	print("2. Rectangle")
	print("3. Square")
	print("4. Parallelogram")
	print("5. Trapezium")
	print("6. Circle")
	print("7. Ellipse")
	print("8. Sector")
	print("")
	o =input("Choose the appropriate option number: ")
	s= ShaeArea()
	if(o==1):
		b= input("Enter value of base: ")
		h=input("Enter value of height: ")
		area = s.areaTriangle(b,h)
		print(area)
	elif(o==2):
		w= input("Enter value of width: ")
		h=input("Enter value of height: ")
		area = s.areaRectangle(w,h)
		print(area)
	elif(o==3):
		a = input("Enter value of side: ")
		area = s.areaSquare(a)
		print(area)
	elif(o==4):
		b= input("Enter value of base: ")
		h=input("Enter value of height: ")
		area = s.areaParallelogram(b,h)
		print(area)
	elif(o==5):
		a = input("Enter value of smaller side: ")
		b= input("Enter value of Longer side: ")
		h= input("Enter value of height: ")
		area = s.areaTrapezium(a,b,h)
		print(area)
	elif(o==6):
		r= input("Enter value of radius: ")
		area = s.areaCircle(r)
		print(area)
	elif(o==7):
		a= input("Enter value of semi major axis: ")
		b= input("Enter value of semi minor axis: ")
		area = s.areaEllipse(a,b)
		print(area)
	elif(o==8):
		r= input("Enter value of radius: ")
		theta= input("Enter value of angle enclosed by sector: ")
		area = s.areaSector(r,theta)
		print(area)
	else:
		print("Invalid Option")
		

class InputError(Exception):
   
    def __init__(self, message):
        self.message = message
main()
