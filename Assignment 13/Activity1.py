def get_name():
    print("Enter name (first last):")
    name = input()
    name = name.strip()
    return name


def get_first(name):
    index = name.find(" ")
    if index < 0:
        first = ""
    else:
        first = name[0:index]
    return first.title()


def get_last(name):
    index = name.find(" ")
    if index < 0:
        last = ""
    else:
        last = name[index + 1:]
        last = last.strip()
    return last.title()


def display_name(first, last):
    print("%s, %s." % (last, first[0]))


def main():
    name = get_name()
    first = get_first(name)
    last = get_last(name)
    display_name(first, last)
    
    
main()
