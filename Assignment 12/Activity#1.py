import java.util.*;
import java.lang.Math;

class JavaApplication {
    private static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        int N;
        
        // Here the function input is called to take the input, number of scores
        N = input_function();
        int[] Scores = new int[N];
        int i;
        
        for (i = 0 ; i <= N - 1 ; i += 1) {
            System.out.println("Enter the Score for student number " + i + 1);
            Scores[i] = input.nextInt();
        }
        
        // Function summary is called by passing the array and its size to calculate the minimum, maximum and average grade.
        summary(Scores);
    }

    private static int input_function() {
        int N;
        
        System.out.println("How many scores you want to enter?");
        N = input.nextInt();
        
        // Return the value N to main function
        
        return N;
    }

    private static void output(int min, int max, int sum, int size) {
        double Average;
        
        // Calculate the average of the grades
        Average = (double) sum / size;
        System.out.println("Minimum Grade : " + min);
        System.out.println("Maximum Grade : " + max);
        System.out.println("Average Grade : " + Average);
    }

    private static void summary(int[] arr) {
        int size;
        
        // Find the size of array using in built function
        size = arr.length;
        int Max;
        
        Max = 1;
        int Min;
        
        // Initialize the min with a veru high value
        Min = 32000;
        int Sum;
        
        Sum = 0;
        int i;
        
        for (i = 0 ; i <= size - 1 ; i += 1) {
            if (arr[i] > Max) {
                // if current value is higher than maximum of past elements, make the new element as max
                Max = arr[i];
            } else {
                if (arr[i] < Min) {
                    Min = arr[i];
                } else {
                    // if current value is lesser than minimum of past elements, make the new element as min
                }
            }
            
            // Sum of all the elements of array
            Sum = Sum + arr[i];
        }
        
        // call the output function to display the result
        output(Min, Max, Sum, size);
    }
}
