# Comment goes here...

def get_iterations():
    print("Enter the number of iterations")
    value = int(input())
    return value


def calculatePi(n):
    result = 3
    s = 1
    for index in range(2, 2 * (n + 1), 2):
        result += s * (4 / (index * (index + 1) * (index + 2)))
        s *= -1
    return result


def print_result(result):
    print("The calculated value of pi is: ")
    print(result)


def main():
    iterations = get_iterations()
    result = calculatePi(iterations)
    print_result(result)


main()