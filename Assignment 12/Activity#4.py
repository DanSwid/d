import random

def sim1():
    count = 0
    
    # Run simulation 100 times
    for i in range(1, 100):
        # Door numbers are assumed to be indices of array. So declared an array of size 3 for 3 doors
        a = [0] * 3
        
        # Initially set all doors to 0
        for j in range(0, 2):
            a[j] = 0
        # Randomly decide which door has car
        CarDoor = int(random.random() * (3))
        a[CarDoor] = 1
        
        # Generate the user's choice
        choice = int(random.random() * (3))
        
        # Check if user made correct choice or not. Based on that increment if choice was correct.
        if a[choice] == 1:
            count = count + 1
    # Display how many correct choices were made in 100 simulations
    print("Choices that were correct : " + str(count))

def sim2():
    count = 0
    for i in range(1, 100):
        a = [0] * 3
        
        for j in range(0, 2):
            a[j] = 0
        CarDoor = int(random.random() * (3))
        a[CarDoor] = 1
        choice = int(random.random() * (3))
        
        # Above this point, its similar to sim1
        # To remove the othere 0 or "goat' door, keep generating the shown option unless its aligns
        while True:    #This simulates a Do Loop
            shown = int(random.random() * (3))
            if not(a[shown] == 1 or shown == choice): break   #Exit loop
        # Check if switching the door was successful or not, if successful, increment the count
        count = count + a[3 - choice - shown]
    # Display how many times users got the correct option by switching
    print("Choices that were correct : " + str(count))

# Main
random.seed()   #Prepare random number generator

print("Result for direct chances of winning")

# Function to run direct simulation to show 1/3rd chance in monty hall problem
sim1()
print("Result when person switched after showing one wrong door")
sim2()

# Function to run simulation for switched case to show 2/3rd success chance in monty hall problem
print("It can be seen that chances incease almost double after switching")
