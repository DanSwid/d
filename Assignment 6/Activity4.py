# Comment goes here ...

def input_radius():
    print("Enter Radius of circle")
    radius = int(input())
    return radius

def input_side():
    print("Enter side of square")
    side = int(input())
    return side

def input_length():
    print("Enter length of rectangle")
    length = int(input())
    return length

def input_breadth():
    print("Enter breadth of rectangle")
    b = int(input())
    return b

def circle_area(radius):
    area = 3.14 * radius * radius
    return area

def rectangle_area(length, breadth):
    area = length * breadth
    return area

def square_area(side):
    area = side * side
    return area

def display_results(circle, square, rectangle):
    print("Circle Area = " + str(circle))
    print("Square Area = " + str(square))
    print("Rectangle Area = " + str(rectangle))

def main():
    radius = input_radius()
    side = input_side()
    length = input_length()
    breadth = input_breadth()
    circle = circle_area(radius)
    square = square_area(side)
    rectangle = rectangle_area(length, breadth)
    display_results(circle, square, rectangle)
    
main()
