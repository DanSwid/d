<?xml version="1.0"?>
<flowgorithm fileversion="2.0">
    <attributes>
        <attribute name="name" value=""/>
        <attribute name="authors" value="Saurabh"/>
        <attribute name="about" value=""/>
        <attribute name="saved" value="2017-03-06 10:09:46 AM"/>
        <attribute name="created" value="U2F1cmFiaDsgU0FVUkFCSC1QQzsgMjAxNy0wMy0wNjsgMDk6MzQ6MDYgQU07IDI3ODY="/>
        <attribute name="edited" value="U2F1cmFiaDsgU0FVUkFCSC1QQzsgMjAxNy0wMy0wNjsgMTA6MDk6NDYgQU07IDI3ODQ="/>
    </attributes>
    <function name="Main" type="None" variable="">
        <parameters/>
        <body>
            <declare name="shape" type="String" array="False" size="" variables="shape"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <output expression="&quot;Enter the shape for which area is to be calculated&quot;"/>
            <input variable="shape"/>
            <assign variable="area" expression="getArea(shape)"/>
            <output expression="&quot;Area of the &quot; &amp; shape &amp; &quot; is &quot; &amp; area"/>
        </body>
    </function>
    <function name="areaCircle" type="Real" variable="area">
        <parameters/>
        <body>
            <declare name="r" type="Real" array="False" size="" variables="r"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <output expression="&quot;Enter the radius of circle&quot;"/>
            <input variable="r"/>
            <assign variable="area" expression="3.14*r*r"/>
        </body>
    </function>
    <function name="areaEllipse" type="Real" variable="area">
        <parameters/>
        <body>
            <output expression="&quot;Enter major and minor axes length&quot;"/>
            <declare name="a" type="Real" array="False" size="" variables="a"/>
            <declare name="b" type="Real" array="False" size="" variables="b"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <input variable="a"/>
            <input variable="b"/>
            <assign variable="area" expression="3.14*a*b"/>
        </body>
    </function>
    <function name="areaParallelogram" type="Real" variable="area">
        <parameters/>
        <body>
            <declare name="b" type="Real" array="False" size="" variables="b"/>
            <declare name="h" type="Real" array="False" size="" variables="h"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <output expression="&quot;Enter base and height of parallelogram&quot;"/>
            <input variable="b"/>
            <input variable="h"/>
            <assign variable="area" expression="b*h"/>
        </body>
    </function>
    <function name="areaRectangle" type="Real" variable="area">
        <parameters/>
        <body>
            <output expression="&quot;Enter the length and breadth&quot;"/>
            <declare name="l" type="Real" array="False" size="" variables="l"/>
            <declare name="b" type="Real" array="False" size="" variables="b"/>
            <input variable="l"/>
            <input variable="b"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <assign variable="area" expression="l*b"/>
        </body>
    </function>
    <function name="areaSector" type="Real" variable="area">
        <parameters/>
        <body>
            <output expression="&quot;Enter R and theta &quot;"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <declare name="r" type="Real" array="False" size="" variables="r"/>
            <declare name="theta" type="Real" array="False" size="" variables="theta"/>
            <input variable="r"/>
            <input variable="theta"/>
            <assign variable="area" expression="(r*r*theta)/2"/>
        </body>
    </function>
    <function name="areaSquare" type="Real" variable="area">
        <parameters/>
        <body>
            <declare name="a" type="Real" array="False" size="" variables="a"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <output expression="&quot;Enter theedge of square&quot;"/>
            <input variable="a"/>
            <assign variable="area" expression="a*a"/>
        </body>
    </function>
    <function name="areaTrapezium" type="Real" variable="area">
        <parameters/>
        <body>
            <output expression="&quot;Enter the lengths of parallel sides&quot;"/>
            <declare name="a" type="Real" array="False" size="" variables="a"/>
            <declare name="b" type="Real" array="False" size="" variables="b"/>
            <declare name="h" type="Real" array="False" size="" variables="h"/>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <input variable="a"/>
            <input variable="b"/>
            <output expression="&quot;Enter height&quot;"/>
            <input variable="h"/>
            <assign variable="area" expression="((a+b)*h)/2"/>
        </body>
    </function>
    <function name="areaTriangle" type="Real" variable="area">
        <parameters/>
        <body>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <declare name="b" type="Real" array="False" size="" variables="b"/>
            <declare name="h" type="Real" array="False" size="" variables="h"/>
            <output expression="&quot;Enter base and height &quot;"/>
            <input variable="b"/>
            <input variable="h"/>
            <assign variable="area" expression="(b*h)/2"/>
        </body>
    </function>
    <function name="getArea" type="Real" variable="area">
        <parameters>
            <parameter name="shape" type="String" array="False"/>
        </parameters>
        <body>
            <declare name="area" type="Real" array="False" size="" variables="area"/>
            <if expression="shape=&quot;triangle&quot;">
                <then>
                    <assign variable="area" expression="areaTriangle()"/>
                </then>
                <else>
                    <if expression="shape=&quot;rectangle&quot;">
                        <then>
                            <assign variable="area" expression="areaRectangle()"/>
                        </then>
                        <else>
                            <if expression="shape=&quot;trapezium&quot;">
                                <then>
                                    <assign variable="area" expression="areaTrapezium()"/>
                                </then>
                                <else>
                                    <if expression="shape=&quot;ellipse&quot;">
                                        <then>
                                            <assign variable="area" expression="areaEllipse()"/>
                                        </then>
                                        <else>
                                            <if expression="shape=&quot;circle&quot;">
                                                <then>
                                                    <assign variable="area" expression="areaCircle()"/>
                                                </then>
                                                <else>
                                                    <if expression="shape=&quot;square&quot;">
                                                        <then>
                                                            <assign variable="area" expression="areaSquare()"/>
                                                        </then>
                                                        <else>
                                                            <if expression="shape=&quot;sector&quot;">
                                                                <then>
                                                                    <assign variable="area" expression="areaSector()"/>
                                                                </then>
                                                                <else>
                                                                    <if expression="shape=&quot;parallelogram&quot;">
                                                                        <then>
                                                                            <assign variable="area" expression="areaParallelogram()"/>
                                                                        </then>
                                                                        <else/>
                                                                    </if>
                                                                </else>
                                                            </if>
                                                        </else>
                                                    </if>
                                                </else>
                                            </if>
                                        </else>
                                    </if>
                                </else>
                            </if>
                        </else>
                    </if>
                </else>
            </if>
        </body>
    </function>
</flowgorithm>
