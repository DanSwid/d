def converter(age, choice):
    result = age * 12
    if choice == "months":
        print("age in months " + str(result))
    else:
        result = age * 365
        if choice == "days":
            print("age in days " + str(result))
        else:
            result = result * 24
            if choice == "hours":
                print("age in hours " + str(result))
            else:
                result = result * 3600
                print("age in seconds " + str(result))

# Main
print("Enter age in  years")
age = int(input())
print("Enter Choice")
choice = input()
converter(age, choice)
