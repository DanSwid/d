def CalculatePay(Hours, Rate):
    Pay = Hours * Rate
    return Pay

def DisplayResults(Pay):
    print("You earned $" + str(Pay))

def GetRate():
    print("Enter your pay rate:")
    Rate = float(input())
    return Rate

def GetHours():
    print("Enter hours worked:")
    Hours = float(input())
    return Hours

# Main
Hours = GetHours()
Rate = GetRate()
Pay = CalculatePay(Hours, Rate)
DisplayResults(Pay)
