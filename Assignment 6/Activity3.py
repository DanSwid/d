# Comment goes here ...

def input_miles():
    print("Enter length in miles:")
    m = int(input())
    return m

def calculate_kilometers(miles):
    k = 1.609 * miles
    return k

def calculate_meters(miles):
    m = miles * 1609.34
    return m

def calculate_centimeters(miles):
    c = 160934 * miles
    return c

def display_results(kilometers, meters, centimeters):
    print("Length in kilometers = " + str(kilometers))
    print("Length in meters = " + str(meters))
    print("Length in centimeters = " + str(centimeters))
    

def main():
    miles = input_miles()
    kilometers = calculate_kilometers(miles)
    meters = calculate_meters(miles)
    centimeters = calculate_centimeters(miles)
    display_results(kilometers, meters, centimeters)
    
main()
