from __future__ import division
import numpy as np
import xml.etree.ElementTree as ET
##getting root
tree = ET.parse('simple.xml')
root = tree.getroot()
##defining arrays
names = []
price = []
descriptions = []
calories = []
values = []
def __str__(self):
    print name
item_number = 0
##extracting the values
for event in root.findall('food'):
    item_number += 1
    name = event.find('name')
    prices = event.find('price')
    description = event.find('description')
    calorie = event.find('calories')
    if name is None:
        print "Error: No name is given for the %d item"%(item_number)
        break
    if description is None:
        print "Error: No Description has been provided for the %d item"%(item_number)
        break
    if calorie is None:
        print "Error: No. of calories are not provided for item %d"%(item_number)
        break
    if prices is None:
        print "Error: Price of the %d item not mentioned"%(item_number)
    
    names.append(name.text)
    
    descriptions.append(description.text)
    calories.append(int(calorie.text))
    money = prices.text
    money_val = money.strip('$')
    values.append(float(money_val))
    price.append(money)
    ##printing menu
    print "%s - %s - %s - %s\n"%(name.text, description.text, calorie.text, prices.text)
    

##printing summary of the menu
print "%d items- %f average calories- $%f averageprice"%(len(names),np.average(calories), np.average(values) )
