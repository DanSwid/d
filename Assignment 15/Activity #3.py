class DayOfWeek:
	def __init__(self,y,m,d):
		self.year =int(y)
		self.month =int(m)
		self.day = int(d)

	def dayAsNumber(self):
		y = self.year
		m =self.month
		q = self.day

		if(m==1 or m==2):
			m =m+12
			y-=1
	
		k= y%100
		j= int(y/100)
		
		h = (q + int(13*(m+1)/5)  + k + int(k/4) + int(j/4) + 5*j )%7
		return h

	def dayAsString(self):
		h = self.dayAsNumber()

		if(h == 0):
			return 'Saturday'
		if(h == 1):
			return 'Sunday'
		if(h == 2):
			return 'Monday'
		if(h == 3):
			return 'Tuesday'
		if(h == 4):
			return 'Wednesday'
		if(h == 5):
			return 'Thursday'
		if(h == 6):
			return 'Friday'

	def dayAsAbbv(self):
		h = self.dayAsNumber()

		if(h == 0):
			return 'Sat'
		if(h == 1):
			return 'Sun'
		if(h == 2):
			return 'Mon'
		if(h == 3):
			return 'Tue'
		if(h == 4):
			return 'Wed'
		if(h == 5):
			return 'Thu'
		if(h == 6):
			return 'Fri'


def main():
	print ("This program find out the day you were born on ")
	y= input("Enter the year you were born in: ")
	m = input("Enter the month of your birth in number: ")
	d= input("Enter the day of the month you were born in: ")

	dow = DayOfWeek(y,m,d)
	#print(dow.dayAsNumber())
	print(dow.dayAsString())
	#print(dow.dayAsAbbv())

main()