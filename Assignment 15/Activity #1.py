class AgeConverter:
	def __init__(self,age):
		if(age<0):
			raise InputError("Invalid: Age less then zero")
		else:
			self.year=age
	def inMonths(self):
		return self.year*12
	def inDays(self):
		return self.year*12*30
	def inHours(self):
		return self.year*12*30*24
	def inSeconds(self):
		return self.year*12*30*24*60


def main():
	age = input("Enter your age in Years: ")
	ac= AgeConverter(age)
	print("Select the timeframe you want to know your age in")
	print("1. Months")
	print("2. Days")
	print("3. Hours")
	print("4. Seconds")
	print("")
	i =input("Chose appropriate option: ")
	if(i==1):
		print(ac.inMonths())
	elif(i==2):
		print(ac.inDays())
	elif(i==3):
		print(ac.inHours())
	elif(i==4):
		print(ac.inSeconds())
	else:
		print("Invalid option")


class InputError(Exception):
   
    def __init__(self, message):
        self.message = message

main()