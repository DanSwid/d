# Comment goes here...

def get_start():
    print("Enter starting value:")
    value = int(input())
    return value
    

def get_end():
    print("Enter ending value:")
    value = int(input())
    return value
    

def print_table(start, end):
    tab = "\t"
    temp = tab + " "
    for i in range(start, end + 1):
        temp = temp + str(i) + tab
    print(temp)
    
    for i in range(start, end + 1):
        temp = "" + str(i) + tab
        for j in range(start, end + 1):
            temp = temp + str(i * j) + tab
        print(temp)


def main():
    start = get_start()
    end = get_end()
    print_table(start, end)


main()