def Average(Marks):
    ave = 0.0
    for a in range(0, len(Marks)):
        ave = ave + Marks[a]
    ave = ave / len(Marks)
    return ave


def Input():
    Value = float(input())
    return Value


def inputInt():
    val = int(input())
    return val


def Max(Marks):
    maxVal = Marks[0]
    for b in range(1, len(Marks)):
        if maxVal < Marks[b]:
            maxVal = Marks[b]
    return maxVal


def Min(Marks):
    minVal = Marks[0]
    for c in range(1, len(Marks)):
        if minVal > Marks[c]:
            minVal = Marks[c]
    return minVal


def get_marks(Marks):
    for n in range(0, len(Marks)):
        print("Enter the grade number...." + str(n + 1))
        mark = Input()
        Marks[n] = mark


def main():
    print("How many grades you want to enter ?")
    X = inputInt()
    Marks = [0] * X
    get_marks(Marks)
    
    avg = Average(Marks)
    print("The average Value is...: " + str(avg))
    maxi = Max(Marks)
    print("The maximum value is... : " + str(maxi))
    mini = Min(Marks)
    print("The minimum value is... : " + str(mini))


main()
