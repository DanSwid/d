def computeAvg(sum, count):
    avg = float(sum) / count
    return avg

def display(avg):
    print("The avg is " + str(avg))

def forSubroutine(count):
    sum = 0
    print("Enter Scores")
    for index in range(0, count - 1):
        temp = input()
        sum = sum + temp
    return sum

def inputCount():
    print("Enter number of scores")
    count = int(input())
    return count

def whileSubroutine(count):
    sum = 0
    print("Enter Scores")
    while count > 0:
        temp = int(input())
        sum = sum + temp
        count = count - 1
    return sum

# Main
numberOfScores = inputCount()
sum1 = forSubroutine(numberOfScores)
sum2 = whileSubroutine(numberOfScores)
forAvg = computeAvg(sum1, numberOfScores)
whileAvg = computeAvg(sum2, numberOfScores)
display(forAvg)
display(whileAvg)
