Module main()
    Declare String shape
    Declare Real area
    
    Display "Enter the shape for which area is to be calculated"
    Input shape
    Set area = getArea(shape)
    Display "Area of the ", shape, " is ", area
End Module

Function Real areaCircle()
    Declare Real r
    Declare Real area
    
    Display "Enter the radius of circle"
    Input r
    Set area = 3.14 * r * r
    
    Return area
End Function

Function Real areaEllipse()
    Display "Enter major and minor axes length"
    Declare Real a
    Declare Real b
    Declare Real area
    
    Input a
    Input b
    Set area = 3.14 * a * b
    
    Return area
End Function

Function Real areaParallelogram()
    Declare Real b
    Declare Real h
    Declare Real area
    
    Display "Enter base and height of parallelogram"
    Input b
    Input h
    Set area = b * h
    
    Return area
End Function

Function Real areaRectangle()
    Display "Enter the length and breadth"
    Declare Real l
    Declare Real b
    
    Input l
    Input b
    Declare Real area
    
    Set area = l * b
    
    Return area
End Function

Function Real areaSector()
    Display "Enter R and theta "
    Declare Real area
    Declare Real r
    Declare Real theta
    
    Input r
    Input theta
    Set area = r * r * theta / 2
    
    Return area
End Function

Function Real areaSquare()
    Declare Real a
    Declare Real area
    
    Display "Enter theedge of square"
    Input a
    Set area = a * a
    
    Return area
End Function

Function Real areaTrapezium()
    Display "Enter the lengths of parallel sides"
    Declare Real a
    Declare Real b
    Declare Real h
    Declare Real area
    
    Input a
    Input b
    Display "Enter height"
    Input h
    Set area = (a + b) * h / 2
    
    Return area
End Function

Function Real areaTriangle()
    Declare Real area
    Declare Real b
    Declare Real h
    
    Display "Enter base and height "
    Input b
    Input h
    Set area = b * h / 2
    
    Return area
End Function

Function Real getArea(String shape)
    Declare Real area
    
    If shape == "triangle" Then
        Set area = areaTriangle()
    Else
        If shape == "rectangle" Then
            Set area = areaRectangle()
        Else
            If shape == "trapezium" Then
                Set area = areaTrapezium()
            Else
                If shape == "ellipse" Then
                    Set area = areaEllipse()
                Else
                    If shape == "circle" Then
                        Set area = areaCircle()
                    Else
                        If shape == "square" Then
                            Set area = areaSquare()
                        Else
                            If shape == "sector" Then
                                Set area = areaSector()
                            Else
                                If shape == "parallelogram" Then
                                    Set area = areaParallelogram()
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End If
    
    Return area
End Function
